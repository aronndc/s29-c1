// Part 2: Simple Server - Node.js

/*
	1. Create an index.js file inside of the activity folder. 
	2.Import the http module using the required directive. Create a variable port and assign it with the value of 8000.
	3. Create a server using the createServer method that will listen in to the port provided above.
	4. Console log in the terminal a message when the server is successfully running.
	5. Create a condition that when the login route, the register route, the homepage and the usersProfile route is accessed, it will print a message specific to the page accessed.
	6. Access each routes to test if it’s working as intended. Save a screenshot for each test.
	7. Create a condition for any other routes that will return an error message.

*/
// Code here:


const http = require("http");
port = 8000;

const server = http.createServer(function(request, response) {
    if (request.url == "/") {
        response.writeHead(200, { "Content-Type": "text/plain" });
        response.end("Welcome to the home page.");
    } else if (request.url == "/login") {
        response.writeHead(200, { "Content-Type": "text/plain" });
        response.end("Welcome to the login page.");
    } else if (request.url == "/registration") {
        response.writeHead(200, { "Content-Type": "text/plain" });
        response.end("Welcome to the registration page.");
    } else if (request.url == "/profile") {
        response.writeHead(200, { "Content-Type": "text/plain" });
        response.end("Welcome to the profile page.");
    } else {
        response.writeHead(404, { "Content-Type": "text/plain" });
        response.end("Sorry! Cannot access the page you are looking for.");
    }
});

server.listen(port);

console.log(`Server is now up and running on port: ${port}`);
